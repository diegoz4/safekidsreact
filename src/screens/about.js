import React, { useState } from "react";
import { View, Text, StyleSheet, Image } from "react-native";

const About = () => {
    return (
        <View style={Styles.container}>
            <View style={Styles.containerImage}>
                <Image style={Styles.img} source={require('../assets/img/logo.png')} />
            </View>
            {/* <View style={Styles.containerData}>
                <Text style={Styles.dataText}>Desarrollado por</Text>
                <View style={Styles.containerData_2}>
                    <Text style={Styles.dataText_2}>Alejandro Argote</Text>
                    <Text style={Styles.dataText_2}>Camilo Zambrano</Text>
                </View>
            </View> */}

        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    containerImage: {
        flexDirection: 'column',
        paddingTop: 2,
        alignItems: 'center',
    },
    img: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        resizeMode: "contain",
        width: 320
    },
    containerData: {
        padding: 20,
        alignItems: 'center'
    },
    dataText: {
        color: '#3AB7E8',
        fontSize: 18,
        fontWeight: 'bold'
    },
    containerData_2: {
        padding: 20,
        alignItems: "flex-start"
    },
    dataText_2: {
        color: '#3AB7E8',
        fontSize: 16,
    }
})

export default About