import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';

import { NavigationContainer } from "@react-navigation/native";

// Components
import HeaderComponent from "../components/headerComponent";
import ItemListComponent from "../components/itemListComponent";

import { getListNanny, addNanny } from "../utils/userService";

// const list = [
//     {
//         key: '1',
//         name: 'Sara Rivas',
//         photo: 'https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTvV7W1JiVpU2sZvf3PHMPCE5_3wligr52gbKXZATFfltvKo_TW&usqp=CAU',
//         state: 'En Línea',
//         abillities: 'Jugar, Molestar, Comer',
//         description: 'Una descripción que describa alguna descripcion bastante larga describiendo las capacidades y demas cosas de la niñera'
//     },
//     {
//         key: '2',
//         name: 'Adriana Salazar',
//         photo: 'https://i.pinimg.com/236x/a8/25/c4/a825c4d7d498fcf24646e4cd7eb70ce3.jpg',
//         state: 'Ocupada',
//         abillities: 'Jugar, Molestar, Comer',
//         description: 'Una descripción que describa'
//     },
//     {
//         key: '3',
//         name: 'Ximena López',
//         photo: 'https://i.pinimg.com/474x/7d/f3/b1/7df3b171dc2a1a7c05f2763dce040884.jpg',
//         state: 'En Línea',
//         abillities: 'Jugar, Molestar, Comer',
//         description: 'Una descripción que describa'
//     },
//     {
//         key: '4',
//         name: 'Sofía Martínez',
//         photo: 'https://i.pinimg.com/originals/c5/d6/a0/c5d6a0061271a3d6a77d4d703bb266b3.jpg',
//         state: 'En Línea',
//         abillities: 'Jugar, Molestar, Comer',
//         description: 'Una descripción que describa'
//     }
// ]

const renderItem = (item, navigation) => {
    return <ItemListComponent
        name={item.name}
        photo={item.photo}
        state={item.state}
        abillities={item.abillities}
        description={item.description}
        phone={item.phone}
        rate={item.rate}
        num_rates={item.num_rates}
        users_rates={item.users_rates}
        id={item.key}
        {...navigation}
    ></ItemListComponent>
}

class Ninieras extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: []
        }
    }

    listAlready = (nannyList) => {
        this.setState(st => ({
            list: st.list = nannyList
        }))
    }

    componentDidMount() {
        getListNanny(this.listAlready)
    }

    render() {
        getListNanny(this.listAlready)
        return (
            <View>
                <FlatList data={this.state.list} renderItem={({ item }) => renderItem(item, this.props.navigation)} />
            </View>
        );
    }
}

export default Ninieras;