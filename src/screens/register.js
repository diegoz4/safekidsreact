import React, { useState } from 'react';
import auth from "@react-native-firebase/auth";
import { addNanny, addParent } from "../utils/userService";
import { Text, View, KeyboardAvoidingView, TextInput, TouchableOpacity, StyleSheet, Image, Keyboard, ToastAndroid } from 'react-native';

const Register = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [repPasword, setRepPasword] = useState('')
    const [nanny, setNanny] = useState(false)
    const [parent, setParent] = useState(true)

    const showMessage = (msj) => {
        ToastAndroid.show(msj, ToastAndroid.LONG)
    }

    const signWithEmail = () => {
        Keyboard.dismiss()

        if (email == '' || password == '') {
            showMessage('Debe proporcionar un email y un password')
            return
        }

        if (password == repPasword) {
            auth().createUserWithEmailAndPassword(email, password)
                .then(rs => {
                    rs.user.sendEmailVerification()
                        .then(() => {
                            if (nanny) {
                                let json = {
                                    key: email,
                                    name: '',
                                    identification: '',
                                    photo: 'https://firebasestorage.googleapis.com/v0/b/safekids-20.appspot.com/o/register-icon.png?alt=media&token=7a55046b-175c-46b7-8528-f6603f9d2b7e',
                                    state: 'Ocupada',
                                    abillities: '',
                                    description: '',
                                    setData: false,
                                    rate: 0,
                                    num_rates: 0,
                                    users_rates: []
                                }

                                addNanny(json).then(() => {
                                    showMessage('Su registro fué exitoso, por favor verifique en la bandeja de entrada o spam de su correo para activar su cuenta')
                                })

                            }

                            if (parent) {
                                let json = {
                                    key: email,
                                    name: '',
                                    identification: '',
                                    photo: 'https://firebasestorage.googleapis.com/v0/b/safekids-20.appspot.com/o/register-icon.png?alt=media&token=7a55046b-175c-46b7-8528-f6603f9d2b7e'
                                }

                                addParent(json).then(() => {
                                    showMessage('Su registro fué exitoso, por favor verifique en la bandeja de entrada o spam de su correo para activar su cuenta')
                                })
                            }

                            setTimeout(() => {
                                navigation.navigate('Login')
                            }, 2000);
                        })
                })
                .catch(error => {
                    if (error.code === 'auth/email-already-in-use') {
                        showMessage('El email ingresado yá se encuentra registrado!');
                    }

                    if (error.code === 'auth/invalid-email') {
                        showMessage('El email ingresado es invalido, por favor verifíque!');
                    }
                });
        } else {
            console.log('76 - Register: ', 'Las contraseñas ingresadas no coninciden!')
        }
    }

    const changeRoll = (type) => {
        if (type == 1) {
            setParent(true)
            setNanny(false)
        }

        if (type == 2) {
            setParent(false)
            setNanny(true)
        }
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#566c79' }} behavior='position'>
            <View style={styles.container}>
                <Image style={styles.img} source={require('../assets/img/logo.png')} />
                <View style={styles.loginContainer}>
                    <TextInput style={styles.textInput}
                        keyboardType='email-address'
                        placeholder='Ingrese su email'
                        autoCompleteType='email'
                        autoCapitalize='none'
                        onChangeText={mail => setEmail(mail)}
                    />

                    <TextInput style={styles.textInput}
                        placeholder='Ingrese su contraseña'
                        secureTextEntry={true}
                        keyboardType='default'
                        autoCapitalize='none'
                        onChangeText={pass => setPassword(pass)}
                    />

                    <TextInput style={styles.textInput}
                        placeholder='Repita su contraseña'
                        secureTextEntry={true}
                        keyboardType='default'
                        autoCapitalize='none'
                        onChangeText={pass => setRepPasword(pass)}
                    />

                    <View style={styles.radios}>
                        <View style={styles.radioParent}>
                            <Text style={styles.textRadio}>Padre</Text>
                            <TouchableOpacity style={styles.circle} onPress={() => changeRoll(1)}>
                                {parent && (<View style={styles.checkedCircle} />)}
                            </TouchableOpacity>
                        </View>
                        <View style={styles.radioNanny}>
                            <Text style={styles.textRadio}>Niñera</Text>
                            <TouchableOpacity style={styles.circle} onPress={() => changeRoll(2)}>
                                {nanny && (<View style={styles.checkedCircle} />)}
                            </TouchableOpacity>
                        </View>
                    </View>

                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity style={[styles.button, styles.buttonRegister]} disabled={email == '' || password == '' || repPasword == ''} onPress={() => signWithEmail()}>
                            <Text style={[styles.textButtons, email == '' || password == '' || repPasword == '' ? styles.textbuttonRegisterDisabled : styles.textbuttonRegister]}>CONFIRMAR</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </View>
        </KeyboardAvoidingView>

    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    img: {
        marginBottom: 20
    },
    loginContainer: {
        flex: 1,
        paddingHorizontal: 30,
        alignContent: 'center'
    },
    buttonsContainer: {
        flex: 1,
        top: 10,
        alignContent: 'center'
    },
    textInput: {
        backgroundColor: 'white',
        borderColor: '#566c79',
        borderRadius: 5,
        marginTop: 10,
        textAlign: 'center',
        borderWidth: 1,
        fontSize: 20,
        height: 50,
    },
    button: {
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 5,
        marginTop: 10,
        fontSize: 50,
        height: 50
    },
    buttonRegister: {
        backgroundColor: 'gray',
    },
    buttonRegisterDisabled: {
        backgroundColor: 'gray',
    },
    buttonPhone: {
        backgroundColor: '#03a9f4'
    },
    textButtons: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    textbuttonRegister: {
        color: 'white',
    },
    textbuttonRegisterDisabled: {
        color: '#95a5a6',
    },
    radios: {
        flex: 1,
        marginVertical: 10,
        paddingVertical: 10,
        flexDirection: 'row',
        height: 30,
    },
    radioParent: {
        flexDirection: 'row',
        left: 10,
        paddingLeft: 20,
        width: 140,
        height: 50,
    },
    radioNanny: {
        flexDirection: 'row',
        left: 10,
        paddingLeft: 20,
        width: 140,
        height: 50,
    },
    textRadio: {
        color: 'white',
        fontSize: 17,
        fontWeight: 'bold'
    },
    circle: {
        height: 25,
        width: 25,
        marginHorizontal: 20,
        borderRadius: 15,
        borderWidth: 2,
        borderColor: '#ACACAC',
        alignItems: 'center',
        justifyContent: 'center',
    },
    checkedCircle: {
        width: 18,
        height: 18,
        borderRadius: 9,
        backgroundColor: '#8bc34a',
    },
})

export default Register;
