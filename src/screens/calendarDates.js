import React, { Component, useState } from "react";
import { addDate, getDates, editDate, removeDate } from '../utils/dateService';
import { View, Text, FlatList, StyleSheet, TouchableHighlight } from "react-native";

import HeaderComponent from "../components/headerComponent";
import ItemDateComponent from "../components/itemDateComponent";

const renderItem = item => {
    return <ItemDateComponent solicitedname={item.username} solicitedcontact={item.usercontact} hour={item.hour} label={item.label} type={item.type} address={item.address} minutes={item.minutesStimated} />
}
class CalendarDates extends Component {
    constructor(props) {
        super(props)
        this.state = {
            list: []
        }
    }

    listAlready = (listDates) => {
        this.setState(st => ({
            list: st.list = listDates
        }))
    }

    componentDidMount() {
        getDates(global.user, this.listAlready)
    }

    render() {
        getDates(global.user, this.listAlready)
        return (
            <View>
                {/* <TouchableHighlight onPress={() =>
                    removeDate('dosorio4@outlook.com-c03', {
                        key: 'c01',
                        nanny: global.user['key'],
                        hour: '09:00 AM',
                        label: 'Cuidado dos niños',
                        type: 'Párbulo',
                        address: 'Mz 11 Cs 4 - La Carolina',
                        minutesStimated: 140
                    })
                }>
                    <Text>datos</Text>
                </TouchableHighlight> */}
                <FlatList data={this.state.list} renderItem={({ item }) => renderItem(item)} />
            </View>
        )
    }
}

const styles = StyleSheet.create({

})

export default CalendarDates