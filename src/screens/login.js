import React, { useState } from "react";
import auth from "@react-native-firebase/auth";
import { getNanny, getParent } from "../utils/userService";
import { Image, Text, View, TouchableOpacity, StyleSheet, TextInput, KeyboardAvoidingView, ToastAndroid, Keyboard } from "react-native";

const Login = ({ navigation }) => {
    const [user, setUser] = useState({});
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('');
    const [confirm, setConfirm] = useState(null);
    const [code, setCode] = useState('');

    const showMessage = (msj) => {
        ToastAndroid.show(msj, ToastAndroid.LONG)
    }

    const loginWithPhoneNumber = async (phoneNumber) => {
        const confirmation = await auth().signInWithPhoneNumber(phoneNumber)
        setConfirm(confirmation)
    }

    const confirmCode = async () => {
        try {
            await confirm.confirm(code)
        } catch (error) {
            console.log('Invalid code')
        }
    }

    const loginWithEmail = () => {
        Keyboard.dismiss()

        if (email == '' || password == '') return

        auth().signInWithEmailAndPassword(email, password)
            .then(res => {
                if (!res.user.emailVerified) {
                    showMessage('La cuenta no ha sido activada aún, por favor revise la bandeja de entrada de su email')
                } else {

                    getParent(email).then((rs) => {
                        if (rs != undefined) {
                            global.user = rs
                            global.user.type = 'parent'
                            console.log('45 - Login: ', global.user)
                            rs.setData ? navigation.navigate("Ninieras") : navigation.navigate("Profile")
                        }
                    })

                    getNanny(email).then((rs) => {
                        if (rs != undefined) {
                            global.user = rs
                            global.user.type = 'nanny'
                            console.log('54 - Loging: ', global.user)
                            rs.setData ? navigation.navigate("CalendarDates") : navigation.navigate("Profile")
                        }
                    })
                }
            })
            .catch((error) => showMessage('No fue posible ingresar, verifique sus credenciales!!'))
    }

    return (
        <KeyboardAvoidingView style={{ flex: 1, backgroundColor: '#566c79' }} behavior='position'>
            <View style={styles.container}>
                <Image style={styles.img} source={require('../assets/img/logo.png')} />
                <View style={styles.loginContainer}>
                    <TextInput style={styles.textInput}
                        keyboardType='email-address'
                        placeholder='Ingrese su email'
                        autoCompleteType='email'
                        autoCapitalize='none'
                        onChangeText={mail => setEmail(mail)}
                    />

                    <TextInput style={styles.textInput}
                        placeholder='Ingrese su contraseña'
                        secureTextEntry={true}
                        keyboardType='default'
                        autoCapitalize='none'
                        onChangeText={pass => setPassword(pass)}
                    />
                    <View style={styles.buttonsContainer}>
                        <TouchableOpacity style={[styles.button, styles.buttonLogin]} disabled={email == '' || password == ''} onPress={() => loginWithEmail()}>
                            <Text style={[styles.textButtons, email == '' || password == '' ? styles.textbuttonLoginDisabled : styles.textbuttonLogin]}>INICIAR SESIÓN</Text>
                        </TouchableOpacity>
                        {/* <TouchableOpacity style={[styles.button, styles.buttonPhone]} onPress={() => loginWithPhoneNumber('+57 317 8066396')} >
                            <Text style={[styles.textButtons, styles.textbuttonRegister]}>INICIAR CON NÚMERO DE TELÉFONO</Text>
                        </TouchableOpacity> */}
                        <TouchableOpacity style={[styles.button, styles.buttonRegister]} onPress={() => navigation.navigate("Register")}>
                            <Text style={[styles.textButtons, styles.textbuttonRegister]}>REGISTRARSE</Text>
                        </TouchableOpacity>
                    </View>
                </View>


            </View>
        </KeyboardAvoidingView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    img: {
        marginBottom: 20
    },
    loginContainer: {
        flex: 1,
        paddingHorizontal: 30,
        alignContent: 'center'
    },
    buttonsContainer: {
        flex: 1,
        top: 10,
        alignContent: 'center'
    },
    textInput: {
        backgroundColor: 'white',
        borderColor: '#566c79',
        borderRadius: 5,
        marginTop: 10,
        textAlign: 'center',
        borderWidth: 1,
        fontSize: 20,
        height: 50,
    },
    button: {
        alignItems: 'center',
        paddingVertical: 15,
        borderRadius: 5,
        marginTop: 10,
        fontSize: 50,
        height: 50
    },
    buttonLogin: {
        backgroundColor: '#8bc34a',
    },
    buttonRegister: {
        backgroundColor: 'gray',
    },
    buttonPhone: {
        backgroundColor: '#03a9f4'
    },
    textButtons: {
        fontSize: 17,
        fontWeight: 'bold'
    },
    textbuttonLogin: {
        color: '#2c3e50',
    },
    textbuttonLoginDisabled: {
        color: '#7f8c8d',
    },
    textbuttonRegister: {
        color: 'white',
    },
})

export default Login