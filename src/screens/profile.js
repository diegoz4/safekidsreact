import React, { useState } from 'react';
import { Text, View, StyleSheet, ImageBackground, TouchableHighlight, Modal, TextInput, ScrollView, ToastAndroid } from 'react-native';
import Icon from "react-native-vector-icons/FontAwesome";
import { editNanny, getNanny, editParent, uploadImage, getParent } from "../utils/userService";
import RNPickerSelect from 'react-native-picker-select';

import ImagePicker from 'react-native-image-picker';


const Profile = ({ params }) => {
    // const id = global.user['key']
    // const type = global.user['type']

    const [showmodal, setShowmodal] = useState(!global.user['setData']);

    const [filePath, setFilePath] = useState({});

    const [name, setName] = useState(global.user['name']);
    const [photo, setPhoto] = useState({ uri: global.user['photo'] });
    const [identification, setIdentification] = useState(global.user['identification']);
    const [phone, setPhone] = useState(global.user['phone']);
    const [skills, setSkills] = useState(global.user['abillities']);
    const [description, setDescription] = useState(global.user['description']);
    const [availability, setAvailability] = useState(global.user['availability']);
    const [state, setState] = useState(global.user['state']);
    const [rate, setRate] = useState(global.user['rate'] | 0);

    const [updName, SetUpdName] = useState('');
    const [updPhoto, setUpdPhoto] = useState('');
    const [updIdentification, SetUpdIdentification] = useState('');
    const [updPhone, SetUpdPhone] = useState('');
    const [updSkills, SetUpdSkills] = useState('');
    const [updDescription, SetUpdDescription] = useState('');
    const [updAvailability, SetUpdAvailability] = useState('');

    const updatePhoto = () => {
        let options = {
            title: 'Actualizar imagen de perfíl',
            takePhotoButtonTitle: 'Tomar desde cámara',
            chooseFromLibraryButtonTitle: 'Seleccionar desde galería',
            cancelButtonTitle: 'Cancelar',
            storageOptions: {
                skipBackup: true,
                path: 'images',
            }
        }

        ImagePicker.showImagePicker(options, response => {
            if (response.didCancel) {
                console.log('47 - Profile: ', 'canceled')
            } else if (response.error) {
                console.log('49 - Porfile, ImagePicker error: ', response.error)
            } else {
                uploadImage(response.path, response.data, global.user.type == 'nanny' ? 'nannies' : 'parents').then(rs => {
                    setPhoto({ uri: rs })
                })
            }
        })
    }

    const updateUser = type => {
        if (type == 'naany') {
            getNanny(global.user['key']).then((rs) => {
                global.user = rs
                global.user.type = type
            })
        }

        if (type == 'parent') {
            getParent(global.user['key']).then((rs) => {
                global.user = rs
                global.user.type = type
            })
        }

        console.log('74 - Profile: ', global.user)
    }

    const showMessage = (msj) => {
        ToastAndroid.show(msj, ToastAndroid.LONG)
    }

    const checkData = () => {
        if (updName == '' || updIdentification == '' || updSkills == '' || updDescription == '' || updAvailability == '' || updPhone == '') return false

        setName(updName)
        setIdentification(updIdentification)
        setSkills(updSkills)
        setDescription(updDescription)
        setAvailability(updAvailability)
        setPhone(updPhone)

        return true
    }

    const setStateProfile = (st) => {
        if (st == null) {
            // setState(global.user['state'])
            return
        }

        setState(st)

        editNanny(global.user['key'], { state: st })
            .then(() => {
                updateUser()
            })
    }

    const setProfile = () => {
        if (!checkData()) {
            showMessage('¡Todos los campos son requeridos!')
            return
        }

        setShowmodal(!showmodal)

        let data = {
            name: updName,
            identification: updIdentification,
            abillities: updSkills,
            description: updDescription,
            availability: updAvailability,
            phone: updPhone,
            setData: true
        }

        editNanny(global.user['key'], data)
            .then(() => {
                showMessage('¡Datos actualizados correctamente!')
                updateUser('nanny')
            })
    }

    const setProfileParent = () => {
        if (updName == '' || updIdentification == '' || updPhone == '') {
            showMessage('¡Todos los campos son requeridos!')
            return
        }

        setName(updName)
        setIdentification(updIdentification)
        setPhone(updPhone)

        setShowmodal(!showmodal)

        let data = {
            name: updName,
            identification: updIdentification,
            phone: updPhone,
            setData: true
        }

        console.log('151 - profile: ', data)

        editParent(global.user['key'], data)
            .then(() => {
                showMessage('¡Datos actualizados correctamente!')
                updateUser('parent')
            })
    }

    if (global.user.type == 'nanny') {
        return (
            <View style={Styles.container}>
                <ScrollView>
                    <Text style={Styles.name}>{name}</Text>
                    <View style={Styles.containerImage}>
                        <ImageBackground style={Styles.profileImage} imageStyle={{ borderRadius: 200 }} source={photo}>
                            <TouchableHighlight style={Styles.setPhoto} onPress={updatePhoto.bind(this)}>
                                <Icon style={Styles.iconCamera} name="camera" size={32} color="#fff8" />
                            </TouchableHighlight>
                        </ImageBackground>
                    </View>
                    <View style={Styles.containerData}>
                        <Text style={Styles.textData}>Estado:</Text>
                        <RNPickerSelect
                            value={state}
                            onValueChange={(st) => setStateProfile(st)}
                            placeholder={{
                                label: 'Cambiar estado',
                                value: null,
                                color: 'black'
                            }}
                            items={[
                                { label: 'Ocupado / Ocupada', value: 'Ocupada' },
                                { label: 'En Línea', value: 'En Linea' }
                            ]}
                        />
                        <View style={Styles.divider} />
                        <Text style={Styles.titleData}>Identificación:</Text>
                        <Text style={Styles.textData}>{identification}</Text>
                        <View style={Styles.divider} />
                        <Text style={Styles.titleData}>Habilidades:</Text>
                        <Text style={Styles.textData}>{skills}</Text>
                        <View style={Styles.divider} />
                        <Text style={Styles.titleData}>Descripción:</Text>
                        <Text style={Styles.textData}>{description}</Text>
                        <View style={Styles.divider} />
                        <Text style={Styles.titleData}>Disponibilidad:</Text>
                        <Text style={Styles.textData}>{availability}</Text>
                        <View style={Styles.divider} />
                        <Text style={Styles.titleData}>Teléfono de contacto:</Text>
                        <Text style={Styles.textData}>{phone}</Text>
                        <View style={Styles.divider} />
                        <Text style={Styles.titleData}>Calificaciones: {global.user['num_rates']} </Text>
                        <View style={Styles.rateOuterContainer}>
                            <Icon name="star-o" size={20} color="#ce2e94" />
                            <Icon name="star-o" size={20} color="#ce2e94" />
                            <Icon name="star-o" size={20} color="#ce2e94" />
                            <Icon name="star-o" size={20} color="#ce2e94" />
                            <Icon name="star-o" size={20} color="#ce2e94" />
                            <View style={[Styles.rateInnerContainer, { width: rate }]}>
                                <Icon name="star" size={20} color="#ce2e94" />
                                <Icon name="star" size={20} color="#ce2e94" />
                                <Icon name="star" size={20} color="#ce2e94" />
                                <Icon name="star" size={20} color="#ce2e94" />
                                <Icon name="star" size={20} color="#ce2e94" />
                            </View>
                            {/* <Text> - { global.user['rate']}% </Text> */}
                        </View>

                    </View>
                    <TouchableHighlight style={Styles.openButton} onPress={() => setShowmodal(true)}>
                        <Text style={Styles.textStyle}>Actualizar datos</Text>
                    </TouchableHighlight>
                </ScrollView>

                {/* Update information modal */}
                <Modal animationType="slide" transparent={true} visible={showmodal}>
                    <View style={Styles.centeredView}>
                        <View style={Styles.modalView}>
                            <Text style={Styles.modalText}>Actualización de Información</Text>

                            <TextInput style={Styles.textInput}
                                placeholder='Nombre completo'
                                autoCapitalize='words'
                                defaultValue={name}
                                onChangeText={tx => SetUpdName(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Número de identificación'
                                keyboardType='number-pad'
                                defaultValue={identification}
                                onChangeText={tx => SetUpdIdentification(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Habilidades'
                                keyboardType='default'
                                defaultValue={skills}
                                onChangeText={tx => SetUpdSkills(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Descripción'
                                keyboardType='default'
                                defaultValue={description}
                                multiline
                                numberOfLines={3}
                                onChangeText={tx => SetUpdDescription(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Disponibilidad'
                                keyboardType='default'
                                defaultValue={availability}
                                multiline
                                numberOfLines={3}
                                onChangeText={tx => SetUpdAvailability(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Teléfono'
                                keyboardType='number-pad'
                                defaultValue={phone}
                                onChangeText={tx => SetUpdPhone(tx)}
                            />

                            <TouchableHighlight
                                style={{ ...Styles.openButton, backgroundColor: "#2196F3" }}
                                onPress={() => setProfile()}>
                                <Text style={Styles.textStyle}>ACTUALIZAR DATOS</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }

    if (global.user.type == 'parent') {

        return (
            <View style={Styles.container}>
                <ScrollView>
                    <Text style={Styles.name}>{name}</Text>
                    <View style={Styles.containerImage}>
                        <ImageBackground style={Styles.profileImage} imageStyle={{ borderRadius: 200 }} source={photo}>
                            <TouchableHighlight style={Styles.setPhoto} onPress={updatePhoto.bind(this)}>
                                <Icon style={Styles.iconCamera} name="camera" size={32} color="#fff8" />
                            </TouchableHighlight>
                        </ImageBackground>
                    </View>
                    <View style={Styles.containerData}>
                        <Text style={Styles.textData}>Identificación:</Text>
                        <Text style={Styles.textData}>{identification}</Text>
                        <View style={Styles.divider} />
                        <Text style={Styles.textData}>Teléfono:</Text>
                        <Text style={Styles.textData}>{phone}</Text>
                        <View style={Styles.divider} />
                    </View>
                    <TouchableHighlight style={Styles.openButton} onPress={() => setShowmodal(true)}>
                        <Text style={Styles.textStyle}>Actualizar datos</Text>
                    </TouchableHighlight>
                </ScrollView>

                {/* Update information modal */}
                <Modal animationType="slide" transparent={true} visible={showmodal}>
                    <View style={Styles.centeredView}>
                        <View style={Styles.modalView}>
                            <Text style={Styles.modalText}>Actualización de Información</Text>

                            <TextInput style={Styles.textInput}
                                placeholder='Nombre completo'
                                autoCapitalize='words'
                                defaultValue={name}
                                onChangeText={tx => SetUpdName(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Número de identificación'
                                keyboardType='number-pad'
                                defaultValue={identification}
                                onChangeText={tx => SetUpdIdentification(tx)}
                            />

                            <TextInput style={Styles.textInput}
                                placeholder='Teléfono de contacto'
                                keyboardType='number-pad'
                                defaultValue={phone}
                                onChangeText={tx => SetUpdPhone(tx)}
                            />

                            <TouchableHighlight
                                style={{ ...Styles.openButton, backgroundColor: "#2196F3" }}
                                onPress={() => setProfileParent()}>
                                <Text style={Styles.textStyle}>ACTUALIZAR DATOS</Text>
                            </TouchableHighlight>
                        </View>
                    </View>
                </Modal>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column'
    },
    containerImage: {
        flexDirection: 'column',
        paddingTop: 2,
        alignItems: 'center',
    },
    profileImage: {
        justifyContent: 'flex-start',
        alignItems: 'stretch',
        resizeMode: 'cover',
        width: 300,
        height: 300,
        borderRadius: 400
    },
    state: {
        top: -220,
        right: 20,
        width: 90,
        borderRadius: 10,
        paddingHorizontal: 15,
        paddingVertical: 3,
        opacity: 0.8,
    },
    onLine: {
        backgroundColor: 'green'
    },
    occupied: {
        backgroundColor: 'red'
    },
    name: {
        color: 'white',
        fontSize: 20,
        fontWeight: 'bold',
        backgroundColor: 'gray',
        opacity: 0.7,
        textAlign: 'center',
        paddingVertical: 10,
    },
    containerData: {
        flexDirection: 'column',
        marginVertical: 30,
        marginHorizontal: 40
    },
    titleData: {
        fontSize: 20,
        fontWeight: 'bold',
        marginBottom: 5
    },
    textData: {
        fontSize: 20,
        marginBottom: 5
    },
    divider: {
        backgroundColor: '#7f8c8d',
        marginVertical: 5,
        height: 2
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 14,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    textInput: {
        backgroundColor: 'white',
        borderColor: '#566c79',
        borderRadius: 4,
        marginTop: 5,
        marginBottom: 5,
        textAlign: 'center',
        borderWidth: 1,
        paddingTop: 5,
        paddingBottom: 5,
        fontSize: 20,
        height: 35,
        width: 280
    },
    openButton: {
        backgroundColor: "purple",
        paddingHorizontal: 30,
        borderRadius: 20,
        elevation: 2,
        padding: 10,
        marginTop: 10,
        marginHorizontal: 20,
        marginBottom: 20
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center",
        fontSize: 20
    },
    setPhoto: {
        width: 60,
        right: 10,
        bottom: 10,
        height: 60,
        borderRadius: 70,
        position: 'absolute',
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'gray',
        opacity: 0.8,
    },
    rateOuterContainer: {
        display: "flex",
        position: "relative",
        flexDirection: "row",
        paddingTop: 8
    },
    rateInnerContainer: {
        position: "absolute",
        display: "flex",
        flexDirection: "row",
        paddingTop: 8,
        overflow: "hidden"
    }
})

export default Profile;
