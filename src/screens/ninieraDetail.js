import React, { useState, useEffect } from "react";
import { View, Text, StyleSheet, ImageBackground, ScrollView, TouchableOpacity, Modal, TextInput } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import RNDateTimePicker from "@react-native-community/datetimepicker";
import { addDate, getConsecutive, getDatesForUser, removeDate } from "../utils/dateService";
import { editNanny } from "../utils/userService";

const NinieraDetail = ({ route, navigation }) => {
  // console.log(route.params);
  const [listDates, setListDates] = useState([]);
  const [date, setDate] = useState('--/--/----');
  const [timeStart, setTimeStart] = useState('--:-- - --:--');
  const [timeEnd, setTimeEnd] = useState('--:-- - --:--');
  const [showDate, setShowDate] = useState(false);
  const [showTimeStart, setShowTimeStart] = useState(false);
  const [showTimeEnd, setShowTimeEnd] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showModalRate, setShowModalRate] = useState(false);

  const [address, setAddress] = useState('');
  const [type, setType] = useState('');
  const [description, setdescription] = useState('');

  const [btnDisable, setBtnDisable] = useState(false);

  // rate stars
  const [rateCalification, setRate] = useState(0);
  var [starRateModal, setStarRateModal] = useState(0);

  const toggleShowModalRate = () => {
    let rs = [...route.params.users_rates].find(e => e === global.user.key)
    rs ? setBtnDisable(true) : setBtnDisable(false)
    setShowModalRate(!showModalRate)
  }

  const toggleShowModal = () => {
    setShowModal(!showModal)
  }

  const onChangeDate = (event, selectedDate) => {
    const currentDate = selectedDate || date;
    setShowDate(Platform.OS === 'ios');
    setDate(currentDate);
    setShowTimeStart(true);
  };

  const onChangeTimeStart = (event, selectedTime) => {
    const currentTime = selectedTime || timeStart;
    setShowTimeStart(Platform.OS === 'ios');
    setTimeStart(currentTime);
    setShowTimeEnd(true)
  };

  const onChangeTimeEnd = (event, selectedTime) => {
    const currentTime = selectedTime || timeEnd;
    setShowTimeEnd(Platform.OS === 'ios');
    setTimeEnd(currentTime);
  };

  const showDatepicker = () => {
    setShowDate(true);
  };

  const showTimepicker = () => {
    setShowTime(true);
  };

  const getDate = () => {
    let dt = new Date(date)
    return (date != '--/--/----')
      ? `${dt.getDate()}/${dt.getMonth() + 1}/${dt.getFullYear()}`
      : date
  }

  const getStartTime = () => {
    return (timeStart != '--:-- - --:--')
      ? timeConverter(timeStart)
      : timeStart
  }

  const getEndTime = () => {
    return (timeEnd != '--:-- - --:--')
      ? timeConverter(timeEnd)
      : timeEnd
  }

  const timeConverter = pTime => {
    let tm = new Date(pTime)
    let nm = tm.getHours() >= 12 ? 'p.m.' : 'a.m.'

    let h = tm.getHours() > 12 ? tm.getHours() % 12 : tm.getHours()
    let hours = h.toString().length == 1 ? `0${h}` : h

    let minutes = tm.getMinutes().toString().length == 1 ? '0' + tm.getMinutes() : tm.getMinutes()

    return `${hours}:${minutes} ${nm}`
  }

  const getState = () => {
    if (route.params.state == 'En Linea') {
      return Styles.onLine
    } else {
      return Styles.occupied
    }
  }

  const generateDate = async () => {
    if (address == '' || type == '' || description == '' || timeEnd == '--:-- - --:--') return

    let consecutive = await getConsecutive(route.params.id)

    let documentId = `${route.params.id}-c0${consecutive}`

    let duration = parseInt(((timeEnd - timeStart) / 1000) / 60)

    addDate(documentId, {
      key: `c0${consecutive}`,
      nanny: route.params.id,
      user: global.user['key'],
      username: global.user['name'],
      usercontact: global.user['phone'],
      date: getDate(),
      hour: getStartTime(),
      label: description,
      type: type,
      address: address,
      minutesStimated: duration
    })
      .then(() => {
        toggleShowModal()
        getDatesForUser(route.params.id, global.user['key'], listAlready)
      })
  }

  const deleteDate = pDate => {
    let dd = `${pDate.nanny}-${pDate.key}`

    removeDate(dd).then(rs => {
      getDatesForUser(route.params.id, global.user['key'], listAlready)
    })
  }

  const listAlready = (listDates) => {
    setListDates(listDates)
  }


  const setStarRate = (pRate) => {
    !btnDisable ? setStarRateModal(pRate) : null
  }

  const addRate = () => {
    let lusr = [...route.params.users_rates]
    let data

    if (route.params.num_rates > 0) {
      if (lusr.find(e => e === global.user.key) === undefined) {
        let q = route.params.rate * route.params.num_rates + (starRateModal * 20)
        let n = route.params.num_rates + 1
        lusr.push(global.user.key)

        data = { rate: q / n, num_rates: n, users_rates: lusr }

        editNanny(route.params.id, data).then(() => toggleShowModalRate())
        navigation.navigate("Ninieras")
      }
    } else {
      lusr.push(global.user.key)
      data = { rate: starRateModal * 20, num_rates: 1, users_rates: lusr }
      editNanny(route.params.id, data).then(() => toggleShowModalRate())
      navigation.navigate("Ninieras")
    }
  }

  useEffect(() => {
    if (listDates.length == 0) getDatesForUser(route.params.id, global.user['key'], listAlready)
  });

  return (
    <ScrollView>
      <View style={Styles.container}>
        <View style={Styles.containerImage}>
          <ImageBackground style={Styles.profileImage} source={{ uri: route.params.photo }}>
            <Text style={[Styles.state, getState()]}>{route.params.state}</Text>
            <Text style={Styles.name}>{route.params.name}</Text>
          </ImageBackground>
        </View>
        <View style={Styles.containerData}>
          <Text style={Styles.titleContainer}>Información</Text>

          <View style={Styles.containerAbilities}>
            {route.params.abillities.split(',').map(e => { return <Text style={Styles.abillity}>{e.trim()}</Text> })}
          </View>
          <Text style={Styles.description}>{route.params.description}</Text>
          <Text style={Styles.description}><Text style={{ fontWeight: 'bold' }}>Tel: </Text>{route.params.phone}</Text>
          <TouchableOpacity style={Styles.rateOuterContainer} onPress={() => toggleShowModalRate()}>
            <Icon name="star-o" size={20} color="#ce2e94" />
            <Icon name="star-o" size={20} color="#ce2e94" />
            <Icon name="star-o" size={20} color="#ce2e94" />
            <Icon name="star-o" size={20} color="#ce2e94" />
            <Icon name="star-o" size={20} color="#ce2e94" />
            <TouchableOpacity style={[Styles.rateInnerContainer, { width: route.params.rate }]} onPress={() => toggleShowModalRate()}>
              <Icon name="star" size={20} color="#ce2e94" />
              <Icon name="star" size={20} color="#ce2e94" />
              <Icon name="star" size={20} color="#ce2e94" />
              <Icon name="star" size={20} color="#ce2e94" />
              <Icon name="star" size={20} color="#ce2e94" />
            </TouchableOpacity>
          </TouchableOpacity>
        </View>
        <View style={Styles.containerDate}>
          <Text style={[Styles.titleContainer, { marginBottom: 3 }]}>Agendar</Text>
          <Text style={Styles.subTitleContainer}>Disponibilidad:</Text>
          <Text> Lunes a Viernes 8 - 12 AM, 2 - 6 PM</Text>
          <Text style={Styles.subTitleContainer}>Citas:</Text>
          <TouchableOpacity onPress={toggleShowModal} >
            <Text>
              Generar cita &nbsp;
              <Icon style={Styles.iconMenu} name="calendar" size={20} color="#ce2e94" />
            </Text>
          </TouchableOpacity>
          {showDate && (
            <RNDateTimePicker
              testID="dateTimePickerDate"
              value={new Date()}
              mode="date"
              is24Hour={false}
              display="spinner"
              onChange={onChangeDate}
            />
          )}
          {showTimeStart && (
            <RNDateTimePicker
              testID="dateTimePickerTimeStart"
              value={new Date()}
              mode="time"
              is24Hour={false}
              display="spinner"
              onChange={onChangeTimeStart}
            />
          )}
          {showTimeEnd && (
            <RNDateTimePicker
              testID="dateTimePickerTimeEnd"
              value={new Date()}
              mode="time"
              is24Hour={false}
              display="spinner"
              onChange={onChangeTimeEnd}
            />
          )}
          {/* <Text>{getDate()}, {getStartTime()} - {getEndTime()}</Text> */}
          {listDates.map((dt) => {
            return <View style={Styles.dateNanny}>
              <Icon style={Styles.iconRemove} name="trash" size={20} color="#ce2e94" onPress={() => deleteDate(dt)} />
              <Text style={Styles.labelDate}>{dt.label}</Text>
              <Text style={Styles.labelDate}>{dt.date}, {dt.hour}</Text>
            </View>
          })}
        </View>
      </View>
      <Modal animationType="slide" transparent={true} visible={showModal}>
        <View style={Styles.layoutModal}>
          <View style={Styles.contentModal}>
            <Text style={Styles.titleModal}>Generar cita</Text>
            <Text style={Styles.closeButton} onPress={toggleShowModal}>X</Text>

            <TextInput style={Styles.textInput}
              placeholder='Ubicación o Dirección'
              autoCapitalize='words'
              onChangeText={tx => setAddress(tx)}
            />

            <TextInput style={Styles.textInput}
              placeholder='Tipo a cuidar Ej: Párbulo'
              onChangeText={tx => setType(tx)}
            />

            <TextInput style={Styles.textInput}
              placeholder='Breve descripción'
              onChangeText={tx => setdescription(tx)}
            />

            <TouchableOpacity onPress={showDatepicker} style={Styles.setDateButton}>
              <Text>
                <Icon style={Styles.iconMenu} name="calendar" size={20} color="#ce2e94" />
                &nbsp;&nbsp;{getDate()}, {getStartTime()} - {getEndTime()}
              </Text>
            </TouchableOpacity>

            <TouchableOpacity onPress={generateDate} style={{ ...Styles.generateButton, backgroundColor: "#ce2e94" }}>
              <Text style={Styles.dateButton}>Agendar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
      <Modal animationType="slide" transparent={true} visible={showModalRate}>
        <View style={Styles.layoutModal}>
          <View style={Styles.contentModal}>
            <Text style={Styles.titleModal}>Callificar a {route.params.name}</Text>
            <Text style={Styles.closeButton} onPress={toggleShowModalRate}>X</Text>

            <TouchableOpacity style={Styles.setDateButton}>
              <View style={Styles.containerModalRate}>
                <Icon style={Styles.iconRateModal} name={starRateModal >= 1 ? 'star' : 'star-o'} size={20} color={btnDisable ? "#b29bb2" : "#ce2e94"} onPress={() => setStarRate(1)} />
                <Icon style={Styles.iconRateModal} name={starRateModal >= 2 ? 'star' : 'star-o'} size={20} color={btnDisable ? "#b29bb2" : "#ce2e94"} onPress={() => setStarRate(2)} />
                <Icon style={Styles.iconRateModal} name={starRateModal >= 3 ? 'star' : 'star-o'} size={20} color={btnDisable ? "#b29bb2" : "#ce2e94"} onPress={() => setStarRate(3)} />
                <Icon style={Styles.iconRateModal} name={starRateModal >= 4 ? 'star' : 'star-o'} size={20} color={btnDisable ? "#b29bb2" : "#ce2e94"} onPress={() => setStarRate(4)} />
                <Icon style={Styles.iconRateModal} name={starRateModal >= 5 ? 'star' : 'star-o'} size={20} color={btnDisable ? "#b29bb2" : "#ce2e94"} onPress={() => setStarRate(5)} />
              </View>
            </TouchableOpacity>

            <TouchableOpacity disabled={btnDisable} onPress={addRate} style={{ ...Styles.modalRateButton, backgroundColor: btnDisable ? "#b29bb2" : "#ce2e94" }}>
              <Text style={Styles.dateButton}>Callificar</Text>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    </ScrollView >
  )
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column'
  },
  containerImage: {
    flexDirection: 'column',
    paddingTop: 20,
    alignItems: 'center'
  },
  profileImage: {
    justifyContent: 'flex-end',
    alignItems: 'stretch',
    resizeMode: 'cover',
    width: 260,
    height: 300,
  },
  state: {
    top: -220,
    right: 20,
    width: 90,
    borderRadius: 10,
    paddingHorizontal: 15,
    paddingVertical: 3,
    opacity: 0.8,
  },
  onLine: {
    backgroundColor: 'green',
    color: 'white',
    fontWeight: 'bold'
  },
  occupied: {
    backgroundColor: 'red',
    color: 'white',
    fontWeight: 'bold'
  },
  name: {
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
    backgroundColor: 'gray',
    opacity: 0.7,
    textAlign: 'center',
    paddingVertical: 10,
  },
  titleContainer: {
    marginBottom: 15,
    fontWeight: 'bold',
    fontSize: 18,
    borderBottomColor: '#223986',
    borderBottomWidth: 2
  },
  subTitleContainer: {
    fontWeight: 'bold',
    marginTop: 10,
    marginBottom: 10
  },
  containerData: {
    marginHorizontal: 20,
    marginBottom: 10,
    padding: 10,
    top: 20,
    borderColor: '#223986',
    borderStyle: 'solid',
    borderRadius: 5,
    borderWidth: 2,
  },
  containerAbilities: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginBottom: 8
  },
  abillity: {
    // marginRight: 10,
    marginRight: 5,
    paddingHorizontal: 10,
    paddingVertical: 3,
    backgroundColor: '#53b5e8',
    color: 'white',
    borderRadius: 15
  },
  containerDate: {
    marginHorizontal: 20,
    marginBottom: 50,
    padding: 10,
    top: 20,
    borderColor: '#223986',
    borderStyle: 'solid',
    borderRadius: 5,
    borderWidth: 2,
  },
  dateNanny: {
    borderColor: '#ce2e94',
    borderWidth: 2,
    marginTop: 10,
    padding: 5,
    borderRadius: 5,
  },
  labelDate: {
    width: '85%'
  },
  iconRemove: {
    // backgroundColor: 'red',
    padding: 10,
    position: 'absolute',
    top: 5,
    right: 5
  },
  layoutModal: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  contentModal: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 14,
    paddingHorizontal: 35,
    paddingVertical: 10,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5
  },
  titleModal: {
    marginBottom: 15,
    fontWeight: 'bold',
    fontSize: 20,
    borderBottomColor: '#223986',
    borderBottomWidth: 2
  },
  closeButton: {
    position: 'absolute',
    fontWeight: 'bold',
    fontSize: 20,
    right: 20,
    top: 10
  },
  textInput: {
    borderColor: '#566c79',
    borderRadius: 4,
    marginTop: 5,
    marginBottom: 5,
    textAlign: 'left',
    borderWidth: 1,
    paddingTop: 5,
    paddingBottom: 5,
    fontSize: 16,
    height: 35,
    width: 258
  },
  setDateButton: {
    marginVertical: 15
  },
  generateButton: {
    backgroundColor: "purple",
    paddingHorizontal: 30,
    borderRadius: 20,
    elevation: 2,
    padding: 10,
    marginTop: 10,
    marginHorizontal: 20,
    marginBottom: 5
  },
  dateButton: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold'
  },
  rateOuterContainer: {
    display: "flex",
    position: "relative",
    flexDirection: "row",
    paddingTop: 8
  },
  rateInnerContainer: {
    position: "absolute",
    display: "flex",
    flexDirection: "row",
    paddingTop: 8,
    overflow: "hidden"
  },
  containerModalRate: {
    display: "flex",
    flexDirection: 'row'
  },
  modalRateButton: {
    paddingHorizontal: 30,
    borderRadius: 20,
    elevation: 2,
    padding: 10,
    marginTop: 10,
    marginHorizontal: 20,
    marginBottom: 5
  },
  iconRateModal: {
    fontSize: 50
  }
})

export default NinieraDetail