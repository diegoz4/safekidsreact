import React, { Component } from "react";
import { View, Text, StyleSheet, TouchableWithoutFeedback } from "react-native";
import Icon from 'react-native-vector-icons/FontAwesome5';

const HeaderComponent = props => {
    const toggleMenu = () => {
        alert('menu')
    }

    const toggleConfig = () => {
        alert('config')
    }

    const test = () => {
        console.log('15 - props data: ', props.data)
    }

    return (
        <View style={styles.container}>
            <TouchableWithoutFeedback onPress={() => {
                props.navigation.openDrawer()
                test()
            }}>
                <Icon style={styles.iconMenu} name="bars" size={30} color="#000" />
            </TouchableWithoutFeedback>
            <Text style={styles.titleHeader}>LISTADO DE NIÑERAS</Text>
            <TouchableWithoutFeedback onPress={toggleConfig}>
                <Icon style={styles.iconSearch} name="search" size={30} color="#000" />
            </TouchableWithoutFeedback>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: '#ecf0f1',
        marginBottom: 4,
        shadowColor: '#000',
        shadowOpacity: 0.01,
        elevation: 4,
        shadowOffset: {
            width: 0,
            height: 6
        }
    },
    iconMenu: {
        paddingHorizontal: 15,
        paddingVertical: 10
    },
    iconSearch: {
        paddingHorizontal: 15
    },
    titleHeader: {
        fontSize: 18
    }
})

export default HeaderComponent