import React, { Component } from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import Login from "../screens/login";
import Ninieras from "../screens/ninieras";

const Tab = createBottomTabNavigator()

const TabsComponent = props => {
    return (
        <Tab.Navigator>
            <Tab.Screen name="Niñeras" component={Ninieras} />
            <Tab.Screen name="Calendario" component={Ninieras} />
            <Tab.Screen name="Login" component={Login} />
        </Tab.Navigator>
    )
}

export default TabsComponent