
import React, { Component } from "react";
import { View, Text, StyleSheet } from "react-native";
import Icon from "react-native-vector-icons/AntDesign";

const calculedTime = (timeStart, totalMins) => {
    let t = toMinutes(timeStart) + totalMins

    let h = toHours(t)
    return h
}

const toMinutes = time => {
    let t = time.split(' ')
    let t2 = t[0].split(':')

    let h = 0

    t[1] == 'AM' ? h = t2[0] : h = parseInt(t2[0]) + 12

    h = h * 60 + parseInt(t2[1])

    return h
}

const toHours = minutes => {
    let h = parseInt(minutes / 60)
    let m = minutes % 60
    let nm = ''

    h >= 12 ? nm = 'PM' : nm = 'AM'
    h > 12 ? h = h % 12 : null

    return h + ':' + m + ' ' + nm
}

const ItemDateComponent = props => {
    return (
        <View style={Styles.container}>
            <View style={Styles.hourDateContainer}>
                <Text style={Styles.hourDate}>Hora de cita: {props.hour}</Text>
            </View>
            <View style={Styles.dataContainer}>
                <View>
                    <Text style={Styles.title}>Solicita: <Text style={Styles.data}>{props.solicitedname}</Text></Text>
                    <Text style={Styles.title}>Contacto: <Text style={Styles.data}>{props.solicitedcontact}</Text></Text>
                    <Text style={Styles.title}>Descripción: <Text style={Styles.data}>{props.label}</Text></Text>
                    <Text style={Styles.title}>Dirección: <Text style={Styles.data}>{props.address}</Text></Text>
                    <Text style={Styles.title}>Estimado: <Text style={Styles.data}>{props.hour} - {calculedTime(props.hour, props.minutes)} </Text></Text>
                </View>
                <View style={Styles.iconContainer}>
                    <Icon name="calendar" size={52} color="purple" />
                </View>
            </View>
        </View>
    )
}

const Styles = StyleSheet.create({
    container: {
        backgroundColor: '#ecf0f1',
        marginHorizontal: 0,
        marginVertical: 4,
        shadowColor: '#000',
        shadowOpacity: 0.02,
        elevation: 5,
    },
    hourDateContainer: {
        paddingVertical: 4,
        alignItems: 'center',
    },
    hourDate: {
        fontSize: 16,
        fontWeight: 'bold',
        color: '#123412',
        paddingVertical: 4,
        borderBottomColor: 'gray',
        borderBottomWidth: StyleSheet.hairlineWidth
    },
    dataContainer: {
        flexDirection: 'row',
        paddingLeft: 20,
        paddingBottom: 10
    },
    title: {
        fontWeight: 'bold'
    },
    data: {
        paddingVertical: 2,
        fontWeight: 'normal'
    },
    iconContainer: {
        position: 'absolute',
        right: 5,
        top: -30
    }
})

export default ItemDateComponent