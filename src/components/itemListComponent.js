import React, { Component } from "react"
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native"
import Icon from "react-native-vector-icons/FontAwesome";

const ItemListComponent = props => {
    const goTo = props => {
        props.navigate("DetailNiniera", {
            id: props.id,
            name: props.name,
            rate: props.rate,
            num_rates: props.num_rates,
            users_rates: props.users_rates,
            photo: props.photo,
            state: props.state,
            phone: props.phone,
            abillities: props.abillities,
            description: props.description,
        })
    }

    const getState = () => {
        if (props.state == 'En Linea') {
            return Styles.onLine
        }
        if (props.state == 'Ocupada') {
            return Styles.occupied
        }
    }

    return (
        <TouchableOpacity onPress={() => { goTo(props) }}>
            <View style={Styles.container}>
                <Image style={Styles.image} source={{ uri: props.photo, }} />
                <View style={Styles.data}>
                    <View style={[Styles.dataItems, Styles.state, getState()]}></View>
                    <Text style={Styles.dataItems}>{props.name}</Text>
                    <Text style={Styles.dataItems}>{props.abillities}</Text>
                    <View style={Styles.rateOuterContainer}>
                        <Icon name="star-o" size={20} color="#ce2e94" />
                        <Icon name="star-o" size={20} color="#ce2e94" />
                        <Icon name="star-o" size={20} color="#ce2e94" />
                        <Icon name="star-o" size={20} color="#ce2e94" />
                        <Icon name="star-o" size={20} color="#ce2e94" />
                        <View style={[Styles.rateInnerContainer, { width: props.rate }]}>
                            <Icon name="star" size={20} color="#ce2e94" />
                            <Icon name="star" size={20} color="#ce2e94" />
                            <Icon name="star" size={20} color="#ce2e94" />
                            <Icon name="star" size={20} color="#ce2e94" />
                            <Icon name="star" size={20} color="#ce2e94" />
                        </View>
                    </View>


                    <Text style={Styles.dataItems, Styles.description} numberOfLines={2} >{props.description}</Text>
                </View>
            </View>
        </TouchableOpacity>
    )
}

const Styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        backgroundColor: '#ecf0f1',
        marginHorizontal: 0,
        marginVertical: 3,
        shadowColor: '#000',
        shadowOpacity: 0.01,
        elevation: 4
    },
    image: {
        width: 70,
        height: 70,
        margin: 10,
        borderRadius: 50
    },
    data: {
        // backgroundColor: 'gray',
        padding: 10,
        flex: 1
    },
    dataItems: {
        marginBottom: 5,
    },
    description: {
        color: 'gray'
    },
    state: {
        width: 15,
        height: 15,
        top: 10,
        left: -75,
        position: 'absolute',
        borderRadius: 15
    },
    onLine: {
        backgroundColor: 'green'
    },
    occupied: {
        backgroundColor: 'red'
    },
    rateOuterContainer: {
        display: "flex",
        position: "relative",
        flexDirection: "row",
        paddingTop: 8
    },
    rateInnerContainer: {
        position: "absolute",
        display: "flex",
        flexDirection: "row",
        paddingTop: 8,
        overflow: "hidden"
    }
})

export default ItemListComponent
