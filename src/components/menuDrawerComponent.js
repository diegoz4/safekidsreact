import React, { Component } from "react";
import { View, Text, Image, StyleSheet, TouchableWithoutFeedback, Button } from "react-native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator, DrawerContentScrollView, DrawerItem } from "@react-navigation/drawer";
import Icon from "react-native-vector-icons/FontAwesome5";

import Animated from "react-native-reanimated";
import Login from "../screens/login";
import Register from "../screens/register";
import Ninieras from "../screens/ninieras";
import NinieraDetail from "../screens/ninieraDetail";
import CalendarDates from "../screens/calendarDates";
import Profile from "../screens/profile";
import About from "../screens/about";

const Drawer = createDrawerNavigator()
const Stack = createStackNavigator()

const Screens = ({ navigation, style }) => {
    return (
        <Animated.View style={[Styles.stack, style]}>
            <Stack.Navigator
                screenOptions={{
                    headerShown: (global.user != null),
                    headerLeft: () => (
                        <TouchableWithoutFeedback onPress={() => navigation.openDrawer()}>
                            <Icon style={Styles.iconMenu} name="bars" size={30} color="#000" />
                        </TouchableWithoutFeedback>
                    )
                }}>
                <Stack.Screen name="Login" component={Login} />
                <Stack.Screen name="Register" component={Register} />
                <Stack.Screen name="Ninieras" component={Ninieras} options={{ title: 'Niñeras' }} />
                <Stack.Screen name="DetailNiniera" component={NinieraDetail} options={{ title: 'Perfíl' }} />
                <Stack.Screen name="Profile" component={Profile} options={{ title: 'Cuenta de usuario' }} />
                <Stack.Screen name="CalendarDates" component={CalendarDates} options={{ title: 'Calendario de citas' }} />
                <Stack.Screen name="About" component={About} />
            </Stack.Navigator>
        </Animated.View>
    )
}

const DrawerContent = props => {
    if (global.user == null) {
        return (
            <DrawerContentScrollView style={Styles.drawerContent} {...props} pinchGestureEnabled={false}>
                <View style={{ backgroundColor: 'gray' }}>
                    <Image style={Styles.menuLogo} source={require('../assets/img/logo.png')} />
                </View>
                <Text style={Styles.titleMainMenu}>Menú</Text>
                {/* <View>
                    <Text style={Styles.titleTitle}>Servicios</Text>
                    <DrawerItem
                        label="Niñeras"
                        icon={() => <Icon name="baby" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Ninieras")}
                    />
                    <DrawerItem
                        label="Calendario niñeras"
                        icon={() => <Icon name="calendar" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Ninieras")}
                    />
                </View>
                <View>
                    <Text style={Styles.titleTitle}>Cuenta</Text>
                    <DrawerItem
                        label="Cuenta de usuario"
                        icon={() => <Icon name="grin-beam-sweat" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Profile")}
                    />
                    <DrawerItem
                        label="Salir"
                        icon={() => <Icon name="sign-out-alt" size={18} color="purple" />}
                        onPress={() => {
                            logout()
                            props.navigation.navigate("Login")
                        }}
                    />
                </View> */}
                <View>
                    <Text style={Styles.titleTitle}>Información</Text>
                    <DrawerItem
                        label="acerca"
                        icon={() => <Icon name="question-circle" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("About")}
                    />
                </View>
            </DrawerContentScrollView>
        )
    }

    if (global.user != null && global.user.type == 'parent') {
        return (
            <DrawerContentScrollView style={Styles.drawerContent} {...props} pinchGestureEnabled={false}>
                <View style={{ backgroundColor: 'gray' }}>
                    <Image style={Styles.menuLogo} source={require('../assets/img/logo.png')} />
                </View>
                <Text style={Styles.titleMainMenu}>Menú</Text>
                <View>
                    <Text style={Styles.titleTitle}>Servicios</Text>
                    <DrawerItem
                        label="Niñeras"
                        icon={() => <Icon name="baby" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Ninieras")}
                    />
                    {/* <DrawerItem
                        label="Calendario niñeras"
                        icon={() => <Icon name="calendar" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Ninieras")}
                    /> */}
                </View>
                <View>
                    <Text style={Styles.titleTitle}>Cuenta</Text>
                    <DrawerItem
                        label="Cuenta de usuario"
                        icon={() => <Icon name="grin-beam-sweat" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Profile")}
                    />
                    <DrawerItem
                        label="Salir"
                        icon={() => <Icon name="sign-out-alt" size={18} color="purple" />}
                        onPress={() => {
                            logout()
                            props.navigation.navigate("Login")
                        }}
                    />
                </View>
                <View>
                    <Text style={Styles.titleTitle}>Información</Text>
                    <DrawerItem
                        label="acerca"
                        icon={() => <Icon name="question-circle" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("About")}
                    />
                </View>
            </DrawerContentScrollView>
        )
    }

    if (global.user != null && global.user.type == 'nanny') {
        return (
            <DrawerContentScrollView style={Styles.drawerContent} {...props} pinchGestureEnabled={false}>
                <View style={{ backgroundColor: 'gray' }}>
                    <Image style={Styles.menuLogo} source={require('../assets/img/logo.png')} />
                </View>
                <Text style={Styles.titleMainMenu}>Menú</Text>
                <View>
                    {/* <Text style={Styles.titleTitle}>Servicios</Text>
                    <DrawerItem
                        label="Niñeras"
                        icon={() => <Icon name="baby" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Ninieras")}
                    /> */}
                    <DrawerItem
                        label="Calendario citas"
                        icon={() => <Icon name="clipboard-list" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("CalendarDates")}
                    />
                    {/* <DrawerItem
                        label="Calendario niñeras"
                        icon={() => <Icon name="calendar" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Ninieras")}
                    /> */}
                </View>
                <View>
                    <Text style={Styles.titleTitle}>Cuenta</Text>
                    <DrawerItem
                        label="Cuenta de usuario"
                        icon={() => <Icon name="grin-beam-sweat" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("Profile")}
                    />
                    <DrawerItem
                        label="Salir"
                        icon={() => <Icon name="sign-out-alt" size={18} color="purple" />}
                        onPress={() => {
                            logout()
                            props.navigation.navigate("Login")
                        }}
                    />
                </View>
                <View>
                    <Text style={Styles.titleTitle}>Información</Text>
                    <DrawerItem
                        label="acerca"
                        icon={() => <Icon name="question-circle" size={18} color="purple" />}
                        onPress={() => props.navigation.navigate("About")}
                    />
                </View>
            </DrawerContentScrollView>
        )
    }
}

const logout = () => {
    console.log('logout')
    global.user = null
}

const MenuDrawerComponent = () => {
    const [progress, setProgres] = React.useState(new Animated.Value(0))

    const scale = Animated.interpolate(progress, {
        inputRange: [0, 1],
        outputRange: [1, 0.8]
    })

    const screenStyles = { transform: [{ scale }] }

    // console.log('209 - Menu drawer: ', global.user)
    return (
        <Drawer.Navigator
            drawerType="slide"
            drawerStyle={{ width: "60%" }}
            sceneContainerStyle={{ backgroundColor: '#4b1860' }}
            drawerContent={props => {
                setProgres(props.progress)
                return <DrawerContent {...props} />
            }}
        >
            <Drawer.Screen name="Screens">
                {props => <Screens {...props} style={screenStyles} />}
            </Drawer.Screen>
        </Drawer.Navigator >
    )
}

const Styles = StyleSheet.create({
    stack: {
        flex: 1
    },
    menuContainer: {
        backgroundColor: 'red',
        marginTop: 0,
        paddingTop: 0,
    },
    drawerContent: {
        flex: 1,
        marginTop: -4,
    },
    menuLogo: {
        resizeMode: 'contain',
        marginLeft: -79,
        paddingLeft: 0,
        height: 124,
    },
    titleMainMenu: {
        backgroundColor: 'purple',
        color: 'white',
        fontSize: 24,
        fontWeight: 'bold',
        marginTop: -5,
        marginBottom: 15,
        paddingVertical: 10,
        paddingLeft: 10,
    },
    titleTitle: {
        paddingLeft: 10,
        fontSize: 18,
        fontWeight: 'bold'
    },
    iconMenu: {
        paddingHorizontal: 15,
        paddingVertical: 10
    }
})

export default MenuDrawerComponent