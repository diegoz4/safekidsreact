import firestore from '@react-native-firebase/firestore';

export function addParent(parent) {
    return firestore().collection('parents').add(parent)
}

export async function getListParents(listRetreived) {
    let listParents = []
    let snapshot = await firestore().collection('parents')
        .orderBy('name')
        .get()

    snapshot.forEach((doc) => listParents.push(doc.data()))

    listRetreived(listParents)
}

const workComplete = () => {
    console.log('added successfully')
}

const workWithError = (error) => {
    console.error(error);
}