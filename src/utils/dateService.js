import firestore from '@react-native-firebase/firestore';

// Nannys Dates
export function addDate(nanny, date) {
    return firestore().collection('dates').doc(nanny).set(date)
}

export async function editDate(keyDate, date) {
    let dateRef = await firestore().collection('dates').doc(keyDate)
    return dateRef.update(date)
}

export async function removeDate(keyDate) {
    let dateRef = await firestore().collection('dates').doc(keyDate)

    dateRef.delete()
}

export async function getDates(nanny, listRetreived) {
    let listDates = []
    let snapshot = await firestore().collection('dates').where('nanny', '==', nanny['key']).get()

    snapshot.forEach(doc => listDates.push(doc.data()));

    listRetreived(listDates)
}

export async function getDatesForUser(nannyId, userId, listRetreived) {
    let listDates = []
    let snapshot = await firestore().collection('dates')
        .where('nanny', '==', nannyId)
        .where('user', '==', userId)
        .get()

    snapshot.forEach(doc => listDates.push(doc.data()));

    listRetreived(listDates)
}

export async function getConsecutive(idNanny) {
    let listDates = []
    let snapshot = await firestore().collection('dates').where('nanny', '==', idNanny).get()

    snapshot.forEach(doc => listDates.push(doc.data()));

    return listDates.length
}