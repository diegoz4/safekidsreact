import firestore, { firebase } from '@react-native-firebase/firestore';
import storage from '@react-native-firebase/storage';
import RNFetchBlob from 'react-native-fetch-blob';

const Blob = RNFetchBlob.polyfill.Blob
const fs = RNFetchBlob.fs
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest
window.blob = Blob

// Nannies
export function addNanny(nanny) {
    return firestore().collection('nannies').doc(nanny['key']).set(nanny)
}

export async function editNanny(key, data) {
    let nannyRef = await firestore().collection('nannies').doc(key)

    return nannyRef.update(data)
}

export async function getNanny(key) {
    let nanny = await firestore().collection('nannies')
        .doc(key)
        .get()
    return nanny.data()
}

export async function getListNanny(listRetreived) {
    let listNannies = []
    let snapshot = await firestore().collection('nannies')
        .orderBy('name')
        .get()

    snapshot.forEach((doc) => listNannies.push(doc.data()))

    listRetreived(listNannies)
}

// Parents
export function addParent(parent) {
    return firestore().collection('parents').doc(parent['key']).set(parent)
}

export async function editParent(key, data) {
    let parentRef = await firestore().collection('parents').doc(key)

    return parentRef.update(data)
}

export async function getParent(key) {
    let parent = await firestore().collection('parents')
        .doc(key)
        .get()

    return parent.data()
}

export async function getListParent(listRetreived) {
    let listNannies = []
    let snapshot = await firestore().collection('parents')
        .orderBy('name')
        .get()

    snapshot.forEach((doc) => listNannies.push(doc.data()))

    listRetreived(listNannies)
}

export async function uploadImage(pathImage, dataImage, typeUser, mime = 'image/jpg') {
    let baseName = `prf_img_${global.user.key.split(/[\.@]/g).join('')}.${pathImage.split('.').pop()}`
    let ref = storage().ref(`profile/${baseName}`);

    return new Promise((resolve, reject) => {
        let uploadBlob = null

        fs.readFile(pathImage, 'base64')
            .then((data) => {
                return Blob.build(data, { type: `${mime};BASE64` })
            })
            .then((blob) => {
                uploadBlob = blob
                return ref.putFile(pathImage)
            })
            .then(() => {
                uploadBlob.close()
                return ref.getDownloadURL()
            })
            .then((url) => {
                firestore().collection(typeUser).doc(global.user.key).update({ photo: url })
                resolve(url)
            })
            .catch(err => {
                console.log('94 - user service: ', err)
                reject(err)
            })
    })
}