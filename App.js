import React, { Component } from 'react';
import { StyleSheet, SafeAreaView } from "react-native";
import { NavigationContainer } from "@react-navigation/native";

import Login from "./src/screens/login";
import MenuDrawerComponent from "./src/components/menuDrawerComponent";

class App extends Component {
  constructor(props) {
    super(props)
    global.user = null
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true });
    this.props.onError(error, info);
  }

  render() {
    return (
      <SafeAreaView style={Styles.container}>
        <NavigationContainer>
          <MenuDrawerComponent />
        </NavigationContainer>
      </SafeAreaView>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1
  }
})

export default App;